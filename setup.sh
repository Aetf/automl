#! /bin/bash
set -ex

SELF_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source $SELF_DIR/common.sh

setup() {
    # am i done
    MARKER=/setup-done
    if [[ -f $MARKER ]]; then
        exit
    fi

    # mount /tmp as tmpfs
    cat > /etc/systemd/system/tmp.mount <<EOF
[Unit]
Description=Temporary Directory /tmp

[Mount]
What=tmpfs
Where=/tmp
Type=tmpfs
Options=mode=1777,strictatime,nosuid,nodev,size=50%,nr_inodes=400k

[Install]
WantedBy=local-fs.target
EOF
    systemctl daemon-reload && systemctl enable --now tmp.mount

    # although we now mount 200G to / directly, we create a /data for compatibility
    mkdir -p /data

    # mount /opt from /data
    cat > /etc/systemd/system/opt.mount <<EOF
[Unit]
Description=Bind /opt to /data/opt

[Mount]
What=/data/opt
Where=/opt
Type=none
Options=bind

[Install]
WantedBy=local-fs.target
EOF
    systemctl daemon-reload && systemctl enable --now opt.mount

    # fix /data permission
    mkdir -p /data
    chgrp -R $PROJ_GROUP /data
    chmod -R g+sw /data

    # fix /nfs permission
    mkdir -p /nfs
    chgrp -R $PROJ_GROUP /nfs
    chmod -R g+sw /nfs

    # remove unused PPAs
    find /etc/apt/sources.list.d/ -type f -print -delete

    # base software
    apt-get update
    apt-get install -y zsh fonts-powerline git git-lfs tmux build-essential cmake gawk htop bmon jq software-properties-common
    apt-get install -y python2  # for geni-get
    apt-get autoremove -y

    # neovim
    add-apt-repository -y ppa:neovim-ppa/stable
    apt-get update -y
    apt-get install -y neovim
    echo "Setting default editor to neovim"
    for exe in vi vim editor; do
        update-alternatives --install /usr/bin/$exe $exe /usr/bin/nvim 60
    done

    # docker
    apt-get install -y \
        apt-transport-https \
        ca-certificates \
        curl \
        gnupg \
        lsb-release
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | gpg --batch --yes --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
    echo "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" > /etc/apt/sources.list.d/docker.list
    apt-get update
    apt-get install -y docker-ce docker-ce-cli containerd.io
    apt-get purge -y docker-compose
    apt-get autoremove -y
    # docker-compose
    curl -s https://api.github.com/repos/docker/compose/releases/latest |
        grep -oP "browser_download_url.*\Khttp.*linux.*x86_64" |
        xargs -n 1 curl -JL -o docker-compose &&
        install -D docker-compose /usr/local/libexec/docker/cli-plugins/docker-compose &&
        rm docker-compose

    # cuda driver
    if lspci | grep -q -i nvidia; then
        apt-get purge -y nvidia* libnvidia*
        # install headers for both current running kernel as well as a generic one that will update when the kernel updates
        apt-get install -y linux-headers-$(uname -r) linux-headers-generic
        apt-get install -y nvidia-headless-470-server nvidia-utils-470-server

        rmmod nouveau || true
        modprobe nvidia || true

        distribution=$(. /etc/os-release;echo $ID$VERSION_ID)
        curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey | apt-key add -
        curl -s -L https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.list > /etc/apt/sources.list.d/nvidia-docker.list
        apt-get update
        apt-get install -y nvidia-docker2
        (cat /etc/docker/daemon.json 2>/dev/null || echo "{}") | jq '. + { "default-runtime": "nvidia" }' > tmp.$$.json && mv tmp.$$.json /etc/docker/daemon.json
        # add gpu as generic resource on node
        nvidia-smi --query-gpu=uuid --format=csv,noheader | while read uuid ; do
            jq --arg value "gpu=$uuid" '."node-generic-resources" |= . + [$value]' < /etc/docker/daemon.json > tmp.$$.json && mv tmp.$$.json /etc/docker/daemon.json
        done
    fi

    # daemon config file after possible installation of cuda driver, as that may change this file
    (cat /etc/docker/daemon.json 2>/dev/null || echo "{}") | jq '. + { "data-root": "/data/docker-data" }' > tmp.$$.json && mv tmp.$$.json /etc/docker/daemon.json
    systemctl restart docker
    # fix docker directory permission
    chown -R root.root /data/docker-data
    chmod -R g-s /data/docker-data

    # default to block traffic to docker
    if ! command -v firewall-cmd &> /dev/null; then
        $SELF_DIR/setup-firewall.sh
        # restart docker so docker zone is added
        systemctl restart docker
    fi
    firewall-cmd --zone=docker --set-target=default --permanent
    firewall-cmd --reload
    # restart docker because firewall reload messes with its network settings
    systemctl restart docker

    # additional software
    curl -s https://api.github.com/repos/BurntSushi/ripgrep/releases/latest |
        grep -oP "browser_download_url.*\Khttp.*amd64.deb" |
        xargs -n 1 curl -JL -o install.deb &&
        dpkg -i install.deb &&
        rm install.deb
    curl -s https://api.github.com/repos/sharkdp/fd/releases/latest |
        grep -oP "browser_download_url.*\Khttp.*amd64.deb" |
        xargs -n 1 curl -JL -o install.deb &&
        dpkg -i install.deb &&
        rm install.deb
    # pueued
    curl -s https://api.github.com/repos/Nukesor/pueue/releases/latest |
        grep -oP "browser_download_url.*\Khttp.*pueue-linux-amd64" |
        xargs -n 1 curl -JL -o pueue &&
        install -D pueue /usr/local/bin/pueue &&
        rm pueue
    curl -s https://api.github.com/repos/Nukesor/pueue/releases/latest |
        grep -oP "browser_download_url.*\Khttp.*pueued-linux-amd64" |
        xargs -n 1 curl -JL -o pueued &&
        install -D pueued /usr/local/bin/pueued &&
        rm pueued
    curl -s https://api.github.com/repos/Nukesor/pueue/releases/latest |
        grep -oP "tarball_url.*\Khttp.*tarball/v[^\"]*" |
        xargs -n 1 curl -JL |
        tar xzf - --strip-components=2 --wildcards '*/utils/pueued.service' &&
        sed -i "s#/usr/bin#/usr/local/bin#g" pueued.service &&
        install -Dm644 pueued.service /etc/systemd/user/pueued.service &&
        rm pueued.service &&
        systemctl --user --global enable pueued
    # procs
    curl -s https://api.github.com/repos/dalance/procs/releases/latest |
        grep -oP "browser_download_url.*\Khttp.*x86_64-lnx.zip" |
        xargs -n 1 curl -JL -o install.zip &&
        unzip -o -d /usr/local/bin install.zip &&
        rm install.zip

    echo "Setting default umask"
    sed -i -E 's/^(UMASK\s+)[0-9]+$/\1002/g' /etc/login.defs

    # python
    echo "Setting up python"
    CONDA_PREFIX=/opt/miniconda3
    curl -JOL 'https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh'
    rm -rf "$CONDA_PREFIX"
    bash Miniconda3-latest-Linux-x86_64.sh -b -p $CONDA_PREFIX
    rm Miniconda3-latest-Linux-x86_64.sh
    cat <<CONDARC > $CONDA_PREFIX/condarc
auto_activate_base: true
channel_priority: strict
channels:
- pytorch
- conda-forge
- defaults
CONDARC

    if ! grep 'conda_setup' /etc/zsh/zshenv; then

        cat <<EOF >> /etc/zsh/zshenv
__conda_setup="\$('/opt/miniconda3/bin/conda' 'shell.zsh' 'hook' 2> /dev/null)"
if [ \$? -eq 0 ]; then
    eval "\$__conda_setup"
else
    if [ -f "/opt/miniconda3/etc/profile.d/conda.sh" ]; then
        . "/opt/miniconda3/etc/profile.d/conda.sh"
    else
        export PATH="/opt/miniconda3/bin:\$PATH"
    fi
fi
EOF

    fi
    ln -sf $CONDA_PREFIX/etc/profile.d/conda.sh /etc/profile.d
    $CONDA_PREFIX/bin/conda install --yes pip ipython jupyter jupyterlab matplotlib cython
    #$CONDA_PREFIX/bin/conda install --yes pytorch=1.5.0 torchvision cudatoolkit=10.2 -c pytorch
    # make sure everyone can install
    chgrp -R $PROJ_GROUP $CONDA_PREFIX
    chmod -R g+w $CONDA_PREFIX

    # install project specific
    if [[ -d /nfs/HpBandSter ]]; then
        $CONDA_PREFIX/bin/pip install -e /nfs/HpBandSter/
    fi

    if [[ -d /nfs/Auto-PyTorch ]]; then
        $CONDA_PREFIX/bin/pip install -r /nfs/Auto-PyTorch/requirements.txt
        $CONDA_PREFIX/bin/pip install openml
        $CONDA_PREFIX/bin/pip install -e /nfs/Auto-PyTorch/
    fi

    if [[ -d /nfs/cifar-automl ]]; then
        $CONDA_PREFIX/bin/pip install hyperopt
    fi

    date > $MARKER
}

init

setup

# setup homes
$SELF_DIR/setup-home.sh "$@"
