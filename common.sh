#! /bin/bash
set -ex

SELF_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# detect whether this is cloudlab or something else
is_cloudlab() {
    [[ $HOST == *.cloudlab.us ]]
}

setup_project_group() {
    if is_cloudlab; then
        local orig_user=${SUDO_USER:-$USER}
        export PROJ_GROUP=$(id -gn $orig_user)
    else
        # use a custom group
        export PROJ_GROUP=proj-PG0
        getent group $PROJ_GROUP || groupadd $PROJ_GROUP
    fi
}

# whoami
echo "Running as $(whoami) with groups ($(groups))"

ensure_root() {
    # am i root now
    if [[ $EUID -ne 0 ]]; then
        echo "Escalating to root with sudo"
        exec sudo /bin/bash "$0" "$@"
    fi
}

update_repo() {
    echo "Updating profile repo"
    if [[ -d "$SELF_DIR" ]]; then
        cd "$SELF_DIR"
        git checkout master

        changed=false
        git remote update && git status -uno | grep -q 'branch is behind' && changed=true
        if $changed; then
            git pull
            echo "Updated successfully, reexec self"
            exec "$0" "$@"
        else
            echo "Up-to-date"
        fi

        chgrp -R $PROJ_GROUP "$SELF_DIR"
        chmod -R g+w "$SELF_DIR"
    fi
}

ensure_rust() {
    if ! command -v cargo &>/dev/null; then
        pushd $(mktemp -d)
        curl -JL 'https://static.rust-lang.org/dist/rust-1.56.1-x86_64-unknown-linux-gnu.tar.gz' | tar xz
        rust-*/install.sh
        popd
        cat > /etc/profile.d/rust.sh <<-'EOF'
		    # Expand $PATH to include the directory where snappy applications go.
		    cargo_bin_path="/opt/cargo/bin"
		    if [ -n "${PATH##*${cargo_bin_path}}" ] && [ -n "${PATH##*${cargo_bin_path}:*}" ]; then
		        export PATH="$PATH:${cargo_bin_path}"
		    fi
		EOF
        CARGO_HOME=/opt/cargo cargo install rust-script
    fi
}

init() {
    ensure_root
    setup_project_group
    update_repo
    ensure_rust
}
