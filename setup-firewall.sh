#! /bin/bash
set -ex

SELF_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source $SELF_DIR/common.sh

init

# Setup firewall
echo "Setup firewall"
apt-get update && apt-get install -y firewalld
firewall-cmd --zone=trusted --add-source=192.168.0.0/16
firewall-cmd --runtime-to-permanent
