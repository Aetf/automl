# make all our compilation on tmpfs
MYTEMP=/dev/shm/peifeng
mkdir -p $MYTEMP

# compile modern git
cd $MYTEMP
wget https://mirrors.edge.kernel.org/pub/software/scm/git/git-2.35.0.tar.gz
tar xvf git-*
cd git-*
./configure --prefix=$MYTEMP/staging
make -j4
make install

# compile modern curl
cd $MYTEMP
wget https://github.com/curl/curl/releases/download/curl-7_81_0/curl-7.81.0.tar.gz
tar xvf curl-*
cd curl-*
./configure --prefix=$MYTEMP/staging --with-openssl
make -j4
make install

# make git and curl available
export PATH=$MYTEMP/staging/bin:$PATH

# make homebrew find our git and curl
export HOMEBREW_NO_ENV_FILTERING=1
# make sure homebrew uses fast compilation
export HOMEBREW_TEMP=$MYTEMP

# download installer
cd $MYTEMP
curl -JOL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh

# install homebrew
export HOMEBREW_FORCE_BREWED_CURL=1
export HOMEBREW_FORCE_BREWED_GIT=1
export HOMEBREW_FORCE_BREWED_CA_CERTIFICATES=1
bash ./install.sh

brew install zsh
