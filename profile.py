"""
A cluster with N nodes attached to a LAN. With an optional NFS server and dataset long-term persistancy.

Hardware: c6420
Image: UBUNTU20-64-STD
"""

import geni.portal as portal
import geni.rspec.pg as rspec

# Only Ubuntu images supported.
imageList = [
    ('urn:publicid:IDN+emulab.net+image+emulab-ops:UBUNTU20-64-STD', 'UBUNTU 20.04'),
    ('urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU18-64-STD', 'UBUNTU 18.04'),
    ('urn:publicid:IDN+wisc.cloudlab.us+image+gaia-PG0:gpu-u1804.node-1', 'UBUNTU 18.04 CUDA 10'),
]

pc = portal.Context()
pc.defineParameter("num_nodes", "Number of nodes", portal.ParameterType.INTEGER, 5, [])
pc.defineParameter("user_name", "user name to run additional setup script",
                   portal.ParameterType.STRING, "peifeng")
pc.defineParameter("setup", "additional setup script",
                   portal.ParameterType.STRING, "setup.sh", advanced=True)
pc.defineParameter("node_hw", "Node hardware to use", portal.ParameterType.NODETYPE, "c6420")
pc.defineParameter("has_nfs", "Whether to include a NFS node", portal.ParameterType.BOOLEAN, False)

pc.defineParameter("os_image", "Select OS image",
                   portal.ParameterType.IMAGE,
                   imageList[0], imageList, advanced=True)

pc.defineParameter("nfs_hw", "NFS hardware to use", portal.ParameterType.NODETYPE, "c8220", advanced=True)
pc.defineParameter("dataset", "Dataset URN backing the NFS storage, leave empty to use an ephermal 200G blockstorage on nfs server",
                   portal.ParameterType.STRING,
                   "", advanced=True)
params = pc.bindParameters()

# Do not change these unless you change the setup scripts too.
nfsServerName = "nfs"
nfsLanName = "nfsLan"
nfsDirectory = "/nfs"

request = pc.makeRequestRSpec()

# add lan
lan = request.LAN(nfsLanName)
lan.best_effort = True
lan.vlan_tagging = True
lan.link_multiplexing = True

# nfs server with special block storage server
if params.has_nfs:
    nfsServer = request.RawPC(nfsServerName)
    nfsServer.disk_image = params.os_image
    nfsServer.hardware_type = params.nfs_hw
    nfsServerInterface = nfsServer.addInterface()
    nfsServerInterface.addAddress(rspec.IPv4Address("192.168.1.250", "255.255.255.0"))
    lan.addInterface(nfsServerInterface)
    nfsServer.addService(rspec.Execute(shell="bash", command="/local/repository/setup-firewall.sh"))
    nfsServer.addService(rspec.Execute(shell="bash", command="/local/repository/nfs-server.sh"))

    # Special node that represents the ISCSI device where the dataset resides
    if params.dataset:
        dsnode = request.RemoteBlockstore("dsnode", nfsDirectory)
        dsnode.dataset = params.dataset
        dslink = request.Link("dslink")
        dslink.addInterface(dsnode.interface)
        dslink.addInterface(nfsServer.addInterface())
        # Special attributes for this link that we must use.
        dslink.best_effort = True
        dslink.vlan_tagging = True
        dslink.link_multiplexing = True
    else:
        bs = nfsServer.Blockstore("nfs-bs", nfsDirectory)
        bs.size = "200GB"

# normal nodes
for i in range(params.num_nodes):
    node = request.RawPC("node-{}".format(i + 1))
    node.disk_image = params.os_image
    node.hardware_type = params.node_hw
    bs = node.Blockstore("bs-{}".format(i + 1), "/data")
    bs.size = "200GB"
    intf = node.addInterface("if1")
    if node.hardware_type == 'r7525':
        # r7525 requires special config to use its normal 25Gbps experimental network
        intf.bandwidth = 25600
    intf.addAddress(rspec.IPv4Address("192.168.1.{}".format(i + 1), "255.255.255.0"))
    lan.addInterface(intf)
    node.addService(rspec.Execute(shell="bash", command="/local/repository/setup-firewall.sh"))
    if params.has_nfs:
        node.addService(rspec.Execute(shell="bash", command="/local/repository/nfs-client.sh"))
    if len(params.setup) > 0:
        node.addService(
            rspec.Execute(
                shell="bash",
                command="/local/repository/{} {}".format(params.setup, params.user_name)
            )
        )

pc.printRequestRSpec(request)
